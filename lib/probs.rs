use ndarray as nd;

// pub const GAMMA: f64 = 200e4 / 1e6; // μs^-1
pub const GAMMA: f64 = 1.0; // μs^-1
pub const NGAMMA: f64 = -GAMMA; // μs^-1

pub fn window(t0: f64, dt: f64, Dt: f64, tau: f64) -> f64 {
    assert!(0.0 <= dt);
    assert!(0.0 <= Dt);
    assert!(dt <= Dt);
    assert!(tau >= 0.0);
    if tau == 0.0 || (t0..=Dt).contains(&(t0 + tau)) {
        0.0
    } else if (t0..=t0 + tau).contains(&Dt) {
        (NGAMMA * (Dt - dt)).exp()
            * (1.0 - (NGAMMA * (t0 + tau - Dt)).exp())
    } else if (Dt..=t0 + tau).contains(&t0) {
        (NGAMMA * (t0 - dt)).exp()
            * (1.0 - (NGAMMA * tau).exp())
    } else if t0 == f64::NEG_INFINITY && tau == f64::INFINITY {
        (NGAMMA * (Dt - dt)).exp()
    } else {
        panic!(
            "window: encountered unhandled case: t0={}, dt={}, Dt={}, tau={}",
            t0, dt, Dt, tau,
        )
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Atom {
    C0,
    C1,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Detector {
    A,
    B,
}

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct State(pub Detector, pub Atom);

#[derive(Copy, Clone, Debug)]
pub struct System {
    /// amplitude of the initial ∣C0, E⟩ state
    pub ae: f64,
    /// amplitude of the initial ∣C1, L⟩ state
    pub al: f64,
    /// relative phase of the above
    pub phi: f64,
    /// reflection amplitude of BS1
    pub r1: f64,
    /// transmission amplitude of BS1
    pub t1: f64,
    /// reflection amplitude of BS2
    pub r2: f64,
    /// transmission amplitude of BS2
    pub t2: f64,
    /// time delay between excitation pulses
    pub dt_pulse: f64,
    /// relative time delay between interferometer arms
    pub dt_mz: f64,
    /// phase rotation in the interferometer
    pub phi_intf: f64,
    /// phase rotation on the atomic states
    pub phi_atom: f64,
    /// detector dark count rate, MHz
    pub dark_rate: f64,
}

impl System {
    pub fn with_ae(mut self, ae: f64) -> Self {
        self.ae = ae;
        self
    }

    pub fn with_al(mut self, al: f64) -> Self {
        self.al = al;
        self
    }

    pub fn with_phi(mut self, phi: f64) -> Self {
        self.phi = phi;
        self
    }

    pub fn with_r1(mut self, r1: f64) -> Self {
        self.r1 = r1;
        self
    }

    pub fn with_t1(mut self, t1: f64) -> Self {
        self.t1 = t1;
        self
    }

    pub fn with_r2(mut self, r2: f64) -> Self {
        self.r2 = r2;
        self
    }

    pub fn with_t2(mut self, t2: f64) -> Self {
        self.t2 = t2;
        self
    }

    pub fn with_dt_pulse(mut self, dt_pulse: f64) -> Self {
        self.dt_pulse = dt_pulse;
        self
    }

    pub fn with_dt_mz(mut self, dt_mz: f64) -> Self {
        self.dt_mz = dt_mz;
        self
    }

    pub fn with_phi_intf(mut self, phi_intf: f64) -> Self {
        self.phi_intf = phi_intf;
        self
    }

    pub fn with_phi_atom(mut self, phi_atom: f64) -> Self {
        self.phi_atom = phi_atom;
        self
    }

    pub fn with_dark_rate(mut self, dark_rate: f64) -> Self {
        self.dark_rate = dark_rate;
        self
    }

    fn coeff(&self, state: State, term: usize) -> f64 {
        use Atom::*;
        use Detector::*;
        match (state, term) {
            (State(A, C0),  0) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C0),  1) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C0),  2) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0),  3) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0),  4) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0),  5) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0),  6) =>  self.t1.powi(2) * self.r2.powi(2),
            (State(A, C0),  7) =>  self.t1.powi(2) * self.r2.powi(2),
            (State(A, C0),  8) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C0),  9) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C0), 10) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0), 11) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0), 12) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0), 13) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C0), 14) =>  self.t1.powi(2) * self.r2.powi(2),
            (State(A, C0), 15) =>  self.t1.powi(2) * self.r2.powi(2),
            //
            (State(B, C0),  0) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C0),  1) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C0),  2) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0),  3) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0),  4) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0),  5) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0),  6) =>  self.t1.powi(2) * self.t2.powi(2),
            (State(B, C0),  7) =>  self.t1.powi(2) * self.t2.powi(2),
            (State(B, C0),  8) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C0),  9) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C0), 10) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0), 11) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0), 12) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0), 13) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C0), 14) =>  self.t1.powi(2) * self.t2.powi(2),
            (State(B, C0), 15) =>  self.t1.powi(2) * self.t2.powi(2),
            //
            (State(A, C1),  0) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C1),  1) => -self.r1.powi(2) * self.t2.powi(2),
            (State(A, C1),  2) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1),  3) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1),  4) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1),  5) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1),  6) =>  self.t1.powi(2) * self.r2.powi(2),
            (State(A, C1),  7) => -self.t1.powi(2) * self.r2.powi(2),
            (State(A, C1),  8) => -self.r1.powi(2) * self.t2.powi(2),
            (State(A, C1),  9) =>  self.r1.powi(2) * self.t2.powi(2),
            (State(A, C1), 10) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1), 11) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1), 12) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1), 13) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(A, C1), 14) => -self.r1.powi(2) * self.t2.powi(2),
            (State(A, C1), 15) =>  self.t1.powi(2) * self.r2.powi(2),
            //
            (State(B, C1),  0) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C1),  1) => -self.r1.powi(2) * self.r2.powi(2),
            (State(B, C1),  2) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1),  3) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1),  4) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1),  5) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1),  6) =>  self.t1.powi(2) * self.t2.powi(2),
            (State(B, C1),  7) => -self.t1.powi(2) * self.t2.powi(2),
            (State(B, C1),  8) => -self.r1.powi(2) * self.r2.powi(2),
            (State(B, C1),  9) =>  self.r1.powi(2) * self.r2.powi(2),
            (State(B, C1), 10) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1), 11) =>  self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1), 12) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1), 13) => -self.r1 * self.r2 * self.t1 * self.t2,
            (State(B, C1), 14) => -self.t1.powi(2) * self.t2.powi(2),
            (State(B, C1), 15) =>  self.t1.powi(2) * self.t2.powi(2),
            _ => 0.0,
        }
    }

    pub fn state_bin_prob(
        &self,
        state: State,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        use Atom::*;
        use Detector::*;
        let s = state;
        let I = window;
        let System {
            ae,
            al,
            phi,
            r1,
            t1,
            r2,
            t2,
            dt_pulse,
            dt_mz,
            phi_intf,
            phi_atom,
            dark_rate: _,
        } = *self;
        if atom_rot {
            0.5 * ae.powi(2) * self.coeff(s, 6)
                * I(t0, 0.0, 0.0, tau)
            + 0.5 * al.powi(2) * self.coeff(s, 15)
                * I(t0, dt_pulse, dt_pulse, tau)
            + 0.5 * ae.powi(2) * self.coeff(s, 0)
                * I(t0, dt_mz, dt_mz, tau)
            + 0.5 * al.powi(2) * self.coeff(s, 9)
                * I(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
            + ae.powi(2) * self.coeff(s, 2) * phi_intf.cos()
                * I(t0, dt_mz / 2.0, dt_mz, tau)
            + al.powi(2) * self.coeff(s, 12) * phi_intf.cos()
                * I(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
            + ae * al * self.coeff(s, 7) * (-phi_atom - phi).cos()
                * I(t0, dt_pulse / 2.0, dt_pulse, tau)
            + ae * al * self.coeff(s, 1) * (-phi_atom - phi).cos()
                * I(t0, dt_pulse / 2.0 + dt_mz, dt_pulse + dt_mz, tau)
            + ae * al * self.coeff(s, 5) * (phi_intf + phi_atom + phi).cos()
                * I(t0, dt_pulse / 2.0 + dt_mz / 2.0, dt_pulse.max(dt_mz), tau)
            + ae * al * self.coeff(s, 4) * (phi_intf - phi_atom - phi).cos()
                * I(t0, dt_pulse / 2.0 + dt_mz / 2.0, dt_pulse + dt_mz, tau)
        } else {
            match state {
                State(A, C0) => {
                    ae.powi(2) * (
                        t1.powi(2) * r2.powi(2)
                            * I(t0, 0.0, 0.0, tau)
                        + r1.powi(2) * t2.powi(2)
                            * I(t0, dt_mz, dt_mz, tau)
                        + 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * I(t0, dt_mz / 2.0, dt_mz, tau)
                    )
                },
                State(B, C0) => {
                    ae.powi(2) * (
                        t1.powi(2) * t2.powi(2)
                            * I(t0, 0.0, 0.0, tau)
                        + r1.powi(2) * r2.powi(2)
                            * I(t0, dt_mz, dt_mz, tau)
                        - 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * I(t0, dt_mz / 2.0, dt_mz, tau)
                    )
                },
                State(A, C1) => {
                    al.powi(2) * (
                        t1.powi(2) * r2.powi(2)
                            * I(t0, dt_pulse, dt_pulse, tau)
                        + r1.powi(2) * t2.powi(2)
                            * I(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
                        + 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * I(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
                    )
                },
                State(B, C1) => {
                    al.powi(2) * (
                        t1.powi(2) * t2.powi(2)
                            * I(t0, dt_pulse, dt_pulse, tau)
                        + r1.powi(2) * r2.powi(2)
                            * I(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
                        - 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * I(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
                    )
                },
            }
        }
    }

    pub(crate) fn state_bin_prob_early(
        &self,
        state: State,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        use Atom::*;
        use Detector::*;
        let s = state;
        let I = window;
        let System {
            ae,
            al: _,
            phi: _,
            r1: _,
            t1,
            r2,
            t2,
            dt_pulse: _,
            dt_mz: _,
            phi_intf: _,
            phi_atom: _,
            dark_rate: _,
        } = *self;
        if atom_rot {
            0.5 * ae.powi(2) * self.coeff(s, 6)
                * I(t0, 0.0, 0.0, tau)
        } else {
            match state {
                State(A, C0) => {
                    ae.powi(2) * (
                        t1.powi(2) * r2.powi(2)
                            * window(t0, 0.0, 0.0, tau)
                    )
                },
                State(B, C0) => {
                    ae.powi(2) * (
                        t1.powi(2) * t2.powi(2)
                            * window(t0, 0.0, 0.0, tau)
                    )
                },
                State(A, C1) => 0.0,
                State(B, C1) => 0.0,
            }
        }
    }

    pub(crate) fn state_bin_prob_mid(
        &self,
        state: State,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        use Atom::*;
        use Detector::*;
        let s = state;
        let I = window;
        let System {
            ae,
            al,
            phi,
            r1,
            t1,
            r2,
            t2,
            dt_pulse,
            dt_mz,
            phi_intf,
            phi_atom,
            dark_rate: _,
        } = *self;
        if atom_rot {
            0.5 * al.powi(2) * self.coeff(s, 15)
                * I(t0, dt_pulse, dt_pulse, tau)
            + 0.5 * ae.powi(2) * self.coeff(s, 0)
                * I(t0, dt_mz, dt_mz, tau)
            + ae.powi(2) * self.coeff(s, 2) * phi_intf.cos()
                * I(t0, dt_mz / 2.0, dt_mz, tau)
            + ae * al * self.coeff(s, 7) * (-phi_atom - phi).cos()
                * I(t0, dt_pulse / 2.0, dt_pulse, tau)
            + ae * al * self.coeff(s, 5) * (phi_intf + phi_atom + phi).cos()
                * I(t0, dt_pulse / 2.0 + dt_mz / 2.0, dt_pulse.max(dt_mz), tau)
        } else {
            match state {
                State(A, C0) => {
                    ae.powi(2) * (
                        r1.powi(2) * t2.powi(2)
                            * window(t0, dt_mz, dt_mz, tau)
                        + 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * window(t0, dt_mz / 2.0, dt_mz, tau)
                    )
                },
                State(B, C0) => {
                    ae.powi(2) * (
                        r1.powi(2) * r2.powi(2)
                            * window(t0, dt_mz, dt_mz, tau)
                        - 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * window(t0, dt_mz / 2.0, dt_mz, tau)
                    )
                },
                State(A, C1) => {
                    al.powi(2) * (
                        t1.powi(2) * r2.powi(2)
                            * window(t0, dt_pulse, dt_pulse, tau)
                    )
                },
                State(B, C1) => {
                    al.powi(2) * (
                        t1.powi(2) * t2.powi(2)
                            * window(t0, dt_pulse, dt_pulse, tau)
                    )
                },
            }
        }
    }

    pub(crate) fn state_bin_prob_late(
        &self,
        state: State,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        use Atom::*;
        use Detector::*;
        let s = state;
        let I = window;
        let System {
            ae,
            al,
            phi,
            r1,
            t1,
            r2,
            t2,
            dt_pulse,
            dt_mz,
            phi_intf,
            phi_atom,
            dark_rate: _,
        } = *self;
        if atom_rot {
            0.5 * al.powi(2) * self.coeff(s, 9)
                * I(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
            + al.powi(2) * self.coeff(s, 12) * phi_intf.cos()
                * I(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
            + ae * al * self.coeff(s, 1) * (-phi_atom - phi).cos()
                * I(t0, dt_pulse / 2.0 + dt_mz, dt_pulse + dt_mz, tau)
            + ae * al * self.coeff(s, 4) * (phi_intf - phi_atom - phi)
                * I(t0, dt_pulse / 2.0 + dt_mz / 2.0, dt_pulse + dt_mz, tau)
        } else {
            match state {
                State(A, C0) => 0.0,
                State(B, C0) => 0.0,
                State(A, C1) => {
                    al.powi(2) * (
                        r1.powi(2) * t2.powi(2)
                            * window(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
                        + 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * window(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
                    )
                },
                State(B, C1) => {
                    al.powi(2) * (
                        r1.powi(2) * r2.powi(2)
                            * window(t0, dt_pulse + dt_mz, dt_pulse + dt_mz, tau)
                        - 2.0 * r1 * r2 * t1 * t2
                            * phi_intf.cos()
                            * window(t0, dt_pulse + dt_mz / 2.0, dt_pulse + dt_mz, tau)
                    )
                },
            }
        }
    }

    pub fn dark_count_prob(&self, tau: f64) -> f64 {
        1.0 - (-self.dark_rate * tau).exp()
    }

    pub fn atom_prob(&self, atom: Atom, atom_rot: bool) -> f64 {
        self.state_bin_prob(
            State(Detector::A, atom),
            atom_rot,
            f64::NEG_INFINITY,
            f64::INFINITY,
        )
        + self.state_bin_prob(
            State(Detector::B, atom),
            atom_rot,
            f64::NEG_INFINITY,
            f64::INFINITY,
        )
    }

    pub fn bin_prob(
        &self,
        detector: Detector,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        self.state_bin_prob(State(detector, Atom::C0), atom_rot, t0, tau)
        + self.state_bin_prob(State(detector, Atom::C1), atom_rot, t0, tau)
    }

    pub fn bin_or_dark_prob(
        &self,
        detector: Detector,
        atom_rot: bool,
        t0: f64,
        tau: f64,
    ) -> f64 {
        let p_bin = self.bin_prob(detector, atom_rot, t0, tau);
        let p_dark = self.dark_count_prob(tau);
        p_bin + p_dark - p_bin * p_dark
    }
}

pub fn fold_monte_carlo<I, F, D>(systems: I, calc: F)
    -> Option<nd::Array<f64, D>>
where
    I: IntoIterator<Item = System>,
    F: Fn(System) -> nd::Array<f64, D>,
    D: nd::Dimension,
{
    let mut iter = systems.into_iter();
    let acc: Option<nd::Array<f64, D>> = iter.next().map(&calc);
    let mut count: usize = acc.is_some().into();
    iter.fold(acc, |acc, sys| { count += 1; acc.map(|a| a + calc(sys)) })
        .map(|s| s / count as f64)
}

