use indexmap::IndexMap;
use num_traits::Float;
use rand::Rng;
use whooie::utils::ExpVal;
use crate::probs::{ Atom, Detector, State, System };

fn sample<I, T, R>(items: I, rng: &mut R) -> T
where
    I: IntoIterator<Item = (T, f64)>,
    R: Rng + ?Sized,
{
    let items: Vec<(T, f64)> = items.into_iter().collect();
    let n: f64 = items.iter().map(|(_, p)| p).sum();
    let r: f64 = rng.gen();
    let mut acc: f64 = 0.0;
    for (t, p) in items.into_iter() {
        if (acc..acc + p / n).contains(&r) {
            return t;
        } else {
            acc += p / n;
        }
    }
    panic!("sample: encountered empty list of items");
}

fn sample_uniform<I, T, R>(items: I, rng: &mut R) -> Option<T>
where
    I: IntoIterator<Item = T>,
    R: Rng + ?Sized,
{
    let items: Vec<T> = items.into_iter().collect();
    let n: f64 = items.len() as f64;
    let r: f64 = rng.gen();
    for (k, t) in items.into_iter().enumerate() {
        if (k as f64 / n .. (k + 1) as f64 / n).contains(&r) {
            return Some(t);
        }
    }
    None
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum DarkCount {
    No,
    Yes,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Count {
    T0(Detector, DarkCount),
    T1(Detector, DarkCount),
    T2(Detector, DarkCount),
}

pub fn draw_time_bin<R>(
    sys: System,
    detector: Detector,
    bins: (f64, f64, f64),
    tau: f64,
    rng: &mut R,
) -> Option<Count>
where R: Rng + ?Sized
{
    let p_t0
        = sys.state_bin_prob(State(detector, Atom::C0), false, bins.0, tau)
        + sys.state_bin_prob(State(detector, Atom::C1), false, bins.0, tau);
    let p_t1
        = sys.state_bin_prob(State(detector, Atom::C0), false, bins.1, tau)
        + sys.state_bin_prob(State(detector, Atom::C1), false, bins.1, tau);
    let p_t2
        = sys.state_bin_prob(State(detector, Atom::C0), false, bins.2, tau)
        + sys.state_bin_prob(State(detector, Atom::C1), false, bins.2, tau);
    // println!("{:.6}; {:.6}; {:.6}", p_t0, p_t1, p_t2);
    let r: f64 = rng.gen();
    sample_uniform(
        [
            (
                (0.0..p_t0).contains(&r),
                rng.gen::<f64>() < sys.dark_count_prob(tau),
            ),
            (
                (p_t0..p_t0 + p_t1).contains(&r),
                rng.gen::<f64>() < sys.dark_count_prob(tau),
            ),
            (
                (p_t0 + p_t1..p_t0 + p_t1 + p_t2).contains(&r),
                rng.gen::<f64>() < sys.dark_count_prob(tau),
            ),
        ]
        .into_iter()
        // .map(|(p, d)| { println!("{}; {}", p, d); (p, d) })
        .enumerate()
        .filter(|(_, (photon, dark_count))| *photon || *dark_count),
        rng,
    )
    .map(|(k, (photon, dark_count))| {
        let wrap
            = match k {
                0 => Count::T0,
                1 => Count::T1,
                2 => Count::T2,
                _ => unreachable!()
            };
        match (photon, dark_count) {
            (true, false) => wrap(detector, DarkCount::No),
            (false, true) => wrap(detector, DarkCount::Yes),
            (true, true) => wrap(
                detector,
                if rng.gen::<bool>() { DarkCount::No } else { DarkCount::Yes },
            ),
            _ => unreachable!()
        }
    })
}

pub fn draw_atom<R>(sys: System, time_bin: Count, rng: &mut R)
    -> Option<Atom>
where R: Rng + ?Sized
{
    match time_bin {
        Count::T0(_, DarkCount::Yes)
        | Count::T1(_, DarkCount::Yes)
        | Count::T2(_, DarkCount::Yes)
        => {
            let r: f64 = rng.gen();
            if r < sys.ae.powi(2) {
                Some(Atom::C0)
            } else if r < sys.ae.powi(2) + sys.al.powi(2) {
                Some(Atom::C1)
            } else {
                None
            }
        },
        Count::T0(det, DarkCount::No) => {
            let p_C0
                = sys.state_bin_prob_early(
                    State(det, Atom::C0),
                    false,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            let p_C1
                = sys.state_bin_prob_early(
                    State(det, Atom::C1),
                    false,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            Some(sample([(Atom::C0, p_C0), (Atom::C1, p_C1)], rng))
        },
        Count::T1(det, DarkCount::No)
        => {
            let p_C0
                = sys.state_bin_prob_mid(
                    State(det, Atom::C0),
                    true,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            let p_C1
                = sys.state_bin_prob_mid(
                    State(det, Atom::C1),
                    true,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            Some(sample([(Atom::C0, p_C0), (Atom::C1, p_C1)], rng))
        },
        Count::T2(det, DarkCount::No) => {
            let p_C0
                = sys.state_bin_prob_late(
                    State(det, Atom::C0),
                    false,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            let p_C1
                = sys.state_bin_prob_late(
                    State(det, Atom::C1),
                    false,
                    f64::NEG_INFINITY,
                    f64::INFINITY,
                );
            Some(sample([(Atom::C0, p_C0), (Atom::C1, p_C1)], rng))
        },
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TimeBin {
    T0,
    T1,
    T2,
}

impl From<Count> for TimeBin {
    fn from(time_bin: Count) -> Self {
        match time_bin {
            Count::T0(..) => Self::T0,
            Count::T1(..) => Self::T1,
            Count::T2(..) => Self::T2,
        }
    }
}

pub fn draw_measurement<R>(
    sys: System,
    detector: Detector,
    bins: (f64, f64, f64),
    tau: f64,
    rng: &mut R,
) -> Option<(Atom, TimeBin)>
where R: Rng + ?Sized
{
    draw_time_bin(sys, detector, bins, tau, rng)
        .and_then(|time_bin| {
            draw_atom(sys, time_bin, rng)
                .map(|atom| (atom, time_bin.into()))
        })
}

pub fn histogram<I, T>(items: I) -> (IndexMap<T, usize>, usize)
where
    I: IntoIterator<Item = T>,
    T: Eq + Ord + std::hash::Hash,
{
    let mut hist = IndexMap::<T, usize>::default();
    let mut overall: usize = 0;
    items.into_iter()
        .for_each(|item| {
            overall += 1;
            if let Some(count) = hist.get_mut(&item) {
                *count += 1;
            } else {
                hist.insert_sorted(item, 1);
            }
        });
    (hist, overall)
}

pub fn histogram_prob<I, T>(items: I) -> (IndexMap<T, f64>, usize)
where
    I: IntoIterator<Item = T>,
    T: Eq + Ord + std::hash::Hash,
{
    let (hist, len) = histogram(items);
    let n = len as f64;
    let hist
        = hist.into_iter()
        .map(|(item, count)| (item, count as f64 / n))
        .collect();
    (hist, len)
}

#[derive(Copy, Clone, Debug)]
pub struct MeasProbs {
    pub mzmz: ExpVal,
    pub mzpz: ExpVal,
    pub pzmz: ExpVal,
    pub pzpz: ExpVal,
    pub x0: ExpVal,
    pub x1: ExpVal,
}

pub fn meas_probs(hist: &IndexMap<(Atom, TimeBin), usize>) -> MeasProbs {
    let c0_t0 = *hist.get(&(Atom::C0, TimeBin::T0)).unwrap_or(&0) as f64;
    let c0_t1 = *hist.get(&(Atom::C0, TimeBin::T1)).unwrap_or(&0) as f64;
    let c0_t2 = *hist.get(&(Atom::C0, TimeBin::T2)).unwrap_or(&0) as f64;
    let c1_t0 = *hist.get(&(Atom::C1, TimeBin::T0)).unwrap_or(&0) as f64;
    let c1_t1 = *hist.get(&(Atom::C1, TimeBin::T1)).unwrap_or(&0) as f64;
    let c1_t2 = *hist.get(&(Atom::C1, TimeBin::T2)).unwrap_or(&0) as f64;
    let nz = c0_t0 + c0_t2 + c1_t0 + c1_t2;
    let nx = c0_t1 + c1_t1;

    let p_mzmz = c0_t0 / nz;
    let err_mzmz = (p_mzmz * (1.0 - p_mzmz) / nz).sqrt();
    let p_mzpz = c0_t2 / nz;
    let err_mzpz = (p_mzpz * (1.0 - p_mzpz) / nz).sqrt();
    let p_pzmz = c1_t0 / nz;
    let err_pzmz = (p_pzmz * (1.0 - p_pzmz) / nz).sqrt();
    let p_pzpz = c1_t2 / nz;
    let err_pzpz = (p_pzpz * (1.0 - p_pzpz) / nz).sqrt();
    let p_x0 = c0_t1 / nx;
    let err_x0 = (p_x0 * (1.0 - p_x0) / nx).sqrt();
    let p_x1 = c1_t1 / nx;
    let err_x1 = (p_x1 * (1.0 - p_x1) / nx).sqrt();

    MeasProbs {
        mzmz: ExpVal::new(p_mzmz, err_mzmz),
        mzpz: ExpVal::new(p_mzpz, err_mzpz),
        pzmz: ExpVal::new(p_pzmz, err_pzmz),
        pzpz: ExpVal::new(p_pzpz, err_pzpz),
        x0: ExpVal::new(p_x0, err_x0),
        x1: ExpVal::new(p_x1, err_x1),
    }
}

pub fn fidelity(meas: MeasProbs) -> ExpVal {
    let MeasProbs { mzmz, mzpz: _, pzmz: _, pzpz, x0, x1 } = meas;
    0.5 * mzmz + 0.5 * pzpz + 0.5 * (x0 - x1).abs()
}

