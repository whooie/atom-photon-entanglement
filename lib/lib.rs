#![allow(dead_code, non_snake_case, non_upper_case_globals)]

pub mod probs;
pub mod monte_carlo;

pub fn qq<T>(x: T) -> T
where T: std::fmt::Debug
{
    println!("{:?}", x);
    x
}

