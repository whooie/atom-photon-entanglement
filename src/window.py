import numpy as np
import whooie.pyplotdefs as pd

GAMMA = 1.0

def I(
    t0: np.ndarray,
    dt: np.ndarray,
    Dt: np.ndarray,
    tau: np.ndarray,
) -> np.ndarray:
    return np.maximum(
        (
            np.zeros(t0.shape, dtype=float)
            + (
                ((tau == 0) | ((t0 <= (t0 + tau)) & ((t0 + tau) <= Dt)))
                * 0.0
            )
            + (
                ((t0 <= Dt) & (Dt <= (t0 + tau)))
                * (np.exp(-GAMMA * (Dt - dt)) * (1 - np.exp(-GAMMA * (t0 + tau - Dt))))
            )
            + (
                ((Dt <= t0) & (t0 <= (t0 + tau)))
                * (np.exp(-GAMMA * (t0 - dt)) * (1 - np.exp(-GAMMA * tau)))
            )
        ),
        np.zeros(t0.shape, dtype=float),
    )

dt = 0.0
Dt = 0.0
tau = np.linspace(0.0, 5 * GAMMA, 500)
t0 = np.linspace(Dt - tau.max(), Dt + tau.max(), 500)

T0, TAU = np.meshgrid(t0, tau)
PI = I(T0, dt, Dt, TAU)
(
    pd.Plotter()
    .colorplot(t0, tau, PI)
    .colorbar()
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$")
    .set_ylabel("$\\tau$")
    .set_title(f"$\\gamma = {GAMMA:.3f}; \\Delta t = {Dt:.3f}; \\delta t = {dt:.3f}$", fontsize="x-small")
    .savefig("window.png")
    .show()
)

