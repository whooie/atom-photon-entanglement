from pathlib import Path
import numpy as np
import whooie.pyplotdefs as pd

datadir = Path("output/darkbin")
data = np.load(str(datadir.joinpath("darkbin.npz")))
t0 = data["t0"]
tau = data["tau"]
p_A = data["p_A"]
p_B = data["p_B"]
phi = data["phi"][0]

(
    pd.Plotter()
    .colorplot(t0, tau, p_A)
    .colorbar()
    .set_clim(0.0, 1.0)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"A; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("darkbin-A.png"))
    .close()
)
(
    pd.Plotter()
    .colorplot(t0, tau, p_B)
    .colorbar()
    .set_clim(0.0, 1.0)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"B; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("darkbin-B.png"))
    .close()
)

