#![allow(dead_code, non_snake_case, non_upper_case_globals)]

#[allow(unused_imports)]
use std::{
    f64::consts::PI,
    path::PathBuf,
};
use ndarray as nd;
use rand::{ Rng, distributions::Distribution, thread_rng };
use statrs::distribution::Normal;
use whooie::{ mkdir, write_npz, utils::ExpVal };
use lib::{ monte_carlo::*, probs::* };

const N: usize = 1_000_000;
const M: f64 = 2.8384644058191703e-25; // kg
const W: f64 = 2.0 * PI * 30e3; // s^-1
const T: f64 = 5e-6; // K
const L: f64 = 1389e-9; // m
const K: f64 = 2.0 * PI / L; // m^-1
const HBAR: f64 = 1.0545718001391127e-34; // kg m^2 s^-1
const KB: f64 = 1.38064852e-23; // J K^-1

const PHI: f64 = 0.0;
const PHI_ROT: f64 = 0.0 * PI;
const DT: f64 = 10.0 / GAMMA;
const TAU: f64 = 10.0 / GAMMA;
const BINS: (f64, f64, f64) = (0.0, DT, 2.0 * DT);

fn doit<R>(
    std_phi: Option<f64>,
    std_dt_mz: Option<f64>,
    std_phi_intf: Option<f64>,
    rng: &mut R,
) -> anyhow::Result<ExpVal>
where R: Rng + ?Sized
{
    let dist_phi
        = std_phi.map(|dphi| Normal::new(0.0, dphi))
        .transpose()?;
    let dist_dt_mz
        = std_dt_mz.map(|dmz| Normal::new(DT, dmz))
        .transpose()?;
    let dist_phi_intf
        = std_phi_intf.map(|dphi| Normal::new(PHI_ROT, dphi))
        .transpose()?;

    let (hist, _)
        = histogram(
            (0..N).filter_map(|_| {
                let system = System {
                    ae: 0.5_f64.sqrt(),
                    al: 0.5_f64.sqrt(),
                    phi:
                        dist_phi.map(|dist| dist.sample(rng))
                        .unwrap_or(PHI),
                    r1: 0.5_f64.sqrt(),
                    t1: 0.5_f64.sqrt(),
                    r2: 0.5_f64.sqrt(),
                    t2: 0.5_f64.sqrt(),
                    dt_pulse: DT,
                    dt_mz:
                        dist_dt_mz.map(|dist| dist.sample(rng))
                        .unwrap_or(DT),
                    phi_intf:
                        dist_phi_intf.map(|dist| dist.sample(rng))
                        .unwrap_or(PHI_ROT),
                    phi_atom: PHI_ROT,
                    dark_rate: GAMMA / 1e6,
                };
                draw_measurement(system, Detector::A, BINS, TAU, rng)
            })
        );
    let probs = meas_probs(&hist);
    let f = fidelity(probs);

    // println!("Histogram:");
    // println!("non-null results: {}", len);
    // for (meas, count) in hist.iter() { println!("{:?} : {}", meas, count); }
    // println!();
    // println!("Probabilities:");
    // println!("-z,-z : {}", probs.mzmz);
    // println!("-z,+z : {}", probs.mzpz);
    // println!("+z,-z : {}", probs.pzmz);
    // println!("+z,+z : {}", probs.pzpz);
    // println!("±x,±x : {}", probs.x0  );
    // println!("±x,∓x : {}", probs.x1  );
    // println!();
    // println!("Fidelity:");
    // println!("F = {}", f);

    Ok(f)
}


fn main() -> anyhow::Result<()> {
    let outdir = PathBuf::from("output/mc");
    mkdir!(outdir);

    let mut rng = thread_rng();

    // let var_x
    //     = 2.0 * HBAR / M / W * (0.5 + 1.0 / ((HBAR * W / KB / T).exp() - 1.0));
    // let std_phi = k * var_x.sqrt() * 0.0;

    // let std_phi: nd::Array1<f64>
    //     = nd::Array::logspace(10.0, -3.0, -0.5, 100);

    // let std_dt_mz: nd::Array1<f64>
    //     = nd::Array::logspace(10.0, (DT / 1e6).log10(), (DT / 10.0).log10(), 100);

    let std_phi_intf: nd::Array1<f64>
        = nd::Array::logspace(10.0, -3.0, -0.5, 100);

    let f: nd::Array1<ExpVal>
        = std_phi_intf.iter()
        .map(|dx| doit(None, None, Some(*dx), &mut rng))
        .collect::<anyhow::Result<_>>()?;
    write_npz!(
        outdir.join("std_phi_intf.npz"),
        arrays: {
            "std_phi_intf" => &std_phi_intf,
            "fidelity" => &f.mapv(|ev| ev.val()),
            "err" => &f.mapv(|ev| ev.err()),
        }
    );

    Ok(())
}

