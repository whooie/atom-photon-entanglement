#![allow(dead_code, non_snake_case, non_upper_case_globals)]

use std::{
    f64::consts::PI,
    path::PathBuf,
};
use ndarray as nd;
use whooie::{ loop_call, mkdir, write_npz, utils::FExtremum };
use lib::probs::{ *, Atom::*, Detector::* };

fn main() {
    let outdir = PathBuf::from("output/defplot");
    mkdir!(outdir);

    let phi: f64 = 0.0 * PI;
    let dt: f64 = 10.0 / GAMMA;
    let atom_rot: bool = true;
    let system = System {
        ae: 0.5_f64.sqrt(),
        al: 0.5_f64.sqrt(),
        phi: 0.0,
        r1: 0.5_f64.sqrt(),
        t1: 0.5_f64.sqrt(),
        r2: 0.5_f64.sqrt(),
        t2: 0.5_f64.sqrt(),
        dt_pulse: dt,
        dt_mz: dt,
        phi_intf: phi,
        phi_atom: phi,
        dark_rate: 0.0,
    };

    let tau = nd::Array1::<f64>::linspace(0.0, 5.0 / GAMMA, 500);
    let t0 = nd::Array1::<f64>::linspace(
        -tau.fmax().unwrap(), 2.0 * dt + tau.fmax().unwrap(), 500);

    let f_A_C0 = |i: Vec<usize>| {
        (system.state_bin_prob(State(A, C0), atom_rot, t0[i[1]], tau[i[0]]),)
    };
    let (p_A_C0,): (nd::ArrayD<f64>,) = loop_call!(
        f_A_C0 => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    let f_B_C0 = |i: Vec<usize>| {
        (system.state_bin_prob(State(B, C0), atom_rot, t0[i[1]], tau[i[0]]),)
    };
    let (p_B_C0,): (nd::ArrayD<f64>,) = loop_call!(
        f_B_C0 => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    let f_A_C1 = |i: Vec<usize>| {
        (system.state_bin_prob(State(A, C1), atom_rot, t0[i[1]], tau[i[0]]),)
    };
    let (p_A_C1,): (nd::ArrayD<f64>,) = loop_call!(
        f_A_C1 => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    let f_B_C1 = |i: Vec<usize>| {
        (system.state_bin_prob(State(B, C1), atom_rot, t0[i[1]], tau[i[0]]),)
    };
    let (p_B_C1,): (nd::ArrayD<f64>,) = loop_call!(
        f_B_C1 => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    write_npz!(
        outdir.join("defplot.npz"),
        arrays: {
            "t0" => &t0,
            "tau" => &tau,
            "p_A_C0" => &p_A_C0,
            "p_B_C0" => &p_B_C0,
            "p_A_C1" => &p_A_C1,
            "p_B_C1" => &p_B_C1,
            "phi" => &nd::array![phi],
        }
    );
}
