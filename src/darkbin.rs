#![allow(dead_code, non_snake_case, non_upper_case_globals)]

use std::{
    f64::consts::PI,
    path::PathBuf,
};
use ndarray as nd;
use whooie::{ loop_call, mkdir, write_npz, utils::FExtremum };
use lib::probs::{ *, Detector::* };

fn main() {
    let outdir = PathBuf::from("output/darkbin");
    mkdir!(outdir);

    let phi: f64 = 0.5 * PI;
    let dt: f64 = 10.0 / GAMMA;
    let system = System {
        ae: 0.5_f64.sqrt(),
        al: 0.5_f64.sqrt(),
        phi: PI,
        r1: 0.5_f64.sqrt(),
        t1: 0.5_f64.sqrt(),
        r2: 0.5_f64.sqrt(),
        t2: 0.5_f64.sqrt(),
        dt_pulse: dt,
        dt_mz: dt,
        phi_intf: phi,
        phi_atom: phi,
        dark_rate: 0.1,
    };

    let tau = nd::Array1::<f64>::linspace(0.0, 5.0 / GAMMA, 500);
    let t0 = nd::Array1::<f64>::linspace(
        -tau.fmax().unwrap(), 2.0 * dt + tau.fmax().unwrap(), 500);

    let f_A = |i: Vec<usize>| {
        (system.bin_or_dark_prob(A, true, t0[i[1]], tau[i[0]]),)
    };
    let (p_A,): (nd::ArrayD<f64>,) = loop_call!(
        f_A => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    let f_B = |i: Vec<usize>| {
        (system.bin_or_dark_prob(B, true, t0[i[1]], tau[i[0]]),)
    };
    let (p_B,): (nd::ArrayD<f64>,) = loop_call!(
        f_B => (p: f64,),
        vars: { tau, t0 },
        printflag: false,
    );

    write_npz!(
        outdir.join("darkbin.npz"),
        arrays: {
            "t0" => &t0,
            "tau" => &tau,
            "p_A" => &p_A,
            "p_B" => &p_B,
            "phi" => &nd::array![phi],
        }
    );
}
