from pathlib import Path
import numpy as np
import whooie.pyplotdefs as pd

datadir = Path("output/defplot")
data = np.load(str(datadir.joinpath("defplot.npz")))
t0 = data["t0"]
tau = data["tau"]
p_A_C0 = data["p_A_C0"]
p_B_C0 = data["p_B_C0"]
p_A_C1 = data["p_A_C1"]
p_B_C1 = data["p_B_C1"]
phi = data["phi"][0]

(
    pd.Plotter()
    .colorplot(t0, tau, p_A_C0)
    .colorbar()
    .set_clim(0.0, 0.25)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"A, C0; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("defplot-A_C0.png"))
    .close()
)
(
    pd.Plotter()
    .colorplot(t0, tau, p_B_C0)
    .colorbar()
    .set_clim(0.0, 0.25)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"B, C0; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("defplot-B_C0.png"))
    .close()
)
(
    pd.Plotter()
    .colorplot(t0, tau, p_A_C1)
    .colorbar()
    .set_clim(0.0, 0.25)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"A, C1; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("defplot-A_C1.png"))
    .close()
)
(
    pd.Plotter()
    .colorplot(t0, tau, p_B_C1)
    .colorbar()
    .set_clim(0.0, 0.25)
    .ggrid().grid(False, which="both")
    .set_xlabel("$t_0$ [$1 / \\gamma$]")
    .set_ylabel("$\\tau$ [$1 / \\gamma$]")
    .set_title(f"B, C1; $\\varphi = {phi / np.pi:.3f} \\pi$")
    .savefig(datadir.joinpath("defplot-B_C1.png"))
    .close()
)

