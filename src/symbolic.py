import sympy as sy
from sympy.functions.special.delta_functions import Heaviside as Theta

aE, aL = sy.symbols("a_E a_L")
t, phi, varphi, varphi_ = sy.symbols("t phi varphi varphi'", real=True)
r1, r2, t1, t2 = sy.symbols("r1 r2 t1 t2", real=True)
gamma, dt_pulse, dt_MZ = sy.symbols("gamma tau_pulse tau_MZ", positive=True)
f = sy.symbols("f", cls=sy.Function)
t0 = sy.symbols("t_0", real=True)
tau = sy.symbols("tau", positive=True)

i = sy.I
eiph = sy.exp(i * varphi)
eif = sy.exp(i * phi)

psi = sy.Matrix([[
    aE * i * r2 * t1 * eiph * f(t),
    aE * t2 * t1 * eiph * f(t),
    aE * i * t2 * r1 * f(t - dt_MZ),
    -aE * r2 * r1 * f(t - dt_MZ),
    aL * eif * i * r2 * t1 * eiph * f(t - dt_pulse),
    aL * eif * t2 * t1 * eiph * f(t - dt_pulse),
    aL * eif * i * t2 * r1 * f(t - dt_pulse - dt_MZ),
    -aL * eif * r2 * r1 * f(t - dt_pulse - dt_MZ),
]]).T

U = sy.kronecker_product(
    sy.eye(2),
    # sy.Matrix([
    #     [1, sy.exp(-i * varphi_)],
    #     [-sy.exp(i * varphi_), 1],
    # ]) / sy.sqrt(2),
    sy.eye(4),
)
psi = U * psi

fsubs = {
    f(t): sy.sqrt(gamma * sy.exp(-gamma * t)) * Theta(t),
    f(t - dt_pulse): sy.sqrt(gamma * sy.exp(-gamma * (t - dt_pulse))) * Theta(t - dt_pulse),
    f(t - dt_MZ): sy.sqrt(gamma * sy.exp(-gamma * (t - dt_MZ))) * Theta(t - dt_MZ),
    f(t - dt_pulse - dt_MZ): sy.sqrt(gamma * sy.exp(-gamma * (t - dt_pulse - dt_MZ))) * Theta(t - dt_pulse - dt_MZ),
}

a_A_C0 = (psi[0] + psi[2]).subs(fsubs).simplify()
a_B_C0 = (psi[1] + psi[3]).subs(fsubs).simplify()
a_A_C1 = (psi[4] + psi[6]).subs(fsubs).simplify()
a_B_C1 = (psi[5] + psi[7]).subs(fsubs).simplify()
print(
f"""
\\begin{{aligned}}
    \\ket{{\\psi'(t)}}
        &= \\left( {sy.latex(a_A_C0)} \\right) \\ket{{A, C_0}}
        \\\\
        &+ \\left( {sy.latex(a_B_C0)} \\right) \\ket{{B, C_0}}
        \\\\
        &+ \\left( {sy.latex(a_A_C1)} \\right) \\ket{{A, C_1}}
        \\\\
        &+ \\left( {sy.latex(a_B_C1)} \\right) \\ket{{B, C_1}}
\\end{{aligned}}
"""[1:-1]
.replace("varphi'", "\\varphi'")
.replace("\\tau_", "\\Delta t_")
.replace("\\theta", "\\Theta")
)

print()

p_A_C0 = (a_A_C0 * a_A_C0.conjugate()).expand()
p_B_C0 = (a_B_C0 * a_B_C0.conjugate()).expand()
p_A_C1 = (a_A_C1 * a_A_C1.conjugate()).expand()
p_B_C1 = (a_B_C1 * a_B_C1.conjugate()).expand()
print(
f"""
\\begin{{aligned}}
    p(A, C_0)
        &= {sy.latex(p_A_C0)}
    \\\\
    p(B, C_0)
        &= {sy.latex(p_B_C0)}
    \\\\
    p(A, C_1)
        &= {sy.latex(p_A_C1)}
    \\\\
    p(B, C_1)
        &= {sy.latex(p_B_C1)}
\\end{{aligned}}
"""[1:-1]
.replace("varphi'", "\\varphi'")
.replace("\\tau_", "\\Delta t_")
.replace("\\theta", "\\Theta")
)

